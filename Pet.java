/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package homework4;

import java.util.Arrays;

/**
 *
 * @author john
 */
public class Pet {
    public String species="unknown";
    public String nickname="unknown";
    public int age=-1;
    public int tricklevel=-1;
    public String[] habits;
    
    public Pet(String name,String species){
    this.nickname=name;
    this.species=species;
    }
    public Pet(String name,String species,int age,int tricklevel,String[] habits){
    this.nickname=name;
    this.species=species;
    this.age=age;
    this.tricklevel=tricklevel;
    this.habits=habits;      
        
    }
    public Pet(){
    
    }
    
    public void eat(){
    System.out.println("I am eating");
    }
    public void respond(){
    System.out.println("Hello, owner. I am - "+nickname+". I miss you!");
    }
    public void foul(){
    System.out.println("I need to cover it up");
    }
    @Override
    public String toString(){
    return "pet=dog{nickname="+nickname+", age="+age+", trickLevel="+tricklevel+", habits="+Arrays.toString(habits)+"}}";
    }
    
    
    
}
